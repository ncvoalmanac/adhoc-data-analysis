# Adhoc Data Analysis

This repository is for a place to store work on any adhoc data analysis tasks that come up.

My suggested method of working is to make a folder in the root of this folder for each task, and then include a readme inside there that describes the project, how to run the scripts, how all the different files link, etc.

Before doing any work, please create a virtual environment and install the "setup.py" file with `python setup.py develop` and also the "requirements.txt" with `pip install -r requirements.txt`.

Before merging any work into master, please ensure that the work can be ran by following the instructions in the README.md.