# Data Extract for ONS

In this folder, we will do any adhoc analysis related to the data extract for ONS.

Currently this has been used for three tasks.

There is the notebook "npish_query.ipynb" that answers some questions from the NPISH team in ONS about our data. The trello card for this task is [AD001](https://trello.com/c/E6rxhdt9/1-ad001-npish-ncvo-query), and that has attached to it all the data that is required. You need to move all these files into the `data/external/` directory inside this folder in order to run the notebook.

There is also the notebook "data_extract.ipynb" that after running will create a CSV file with the prefix `account_sample` which needs to be sent to ONS. To use this notebook, ensure the variables specified in the set up section are correct, in particular the variables and `almanac` and `financial_year` need to be updated each time. 

There is also the notebook "ons_query.ipynb" that looks into why the values are so different for the data that we have given ONS this year compared to previously. Before running, this requires the file "accounts_sample_2018-19_2021-08-10.csv" in the "data/external" directory, which can be found on the G drive (under "RESEARCH/Projects/Current projects/Data extract for ONS/2021 07 Extract/ons_almanac_201819_sent_2021-0810")

