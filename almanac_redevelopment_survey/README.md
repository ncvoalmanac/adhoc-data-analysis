# Almanac Redevelopment Survey Analysis

In this folder we will analyse the results of the almanac redevelopment survey.

The structure of the folder is as follows:
* `scripts` - R scripts that are used for either data processing or analytics purposes.
* `data` - any data is saved here. The structure is as follows:
  * `raw` - any raw data that is required to do analysis is saved here, so this is where the survey results are saved.
  * `interim` - where we process the raw data so that it is an easier form to analyse. We have one file for each question responses.
* `summary.md` - a summary of what I have seen from going through the different bits of data.

So far, I have analysed the responses of the free text questions. This analysis is found is in the `summary.md` file. The free text questions are as follows:
* Q10, what kinds of data do you use specifically (e.g. topics, particular variables) (64 answers)
* Q11, when you want to access data about the voluntary sector where do you go? (62 answers)
* Q19, what have you used the Almanac for previously (33 answers)
* Q21, what data did you need that you were unable to find when you used the almanac before (26 answers)
* Q23, how could we make the almanac more useful to you (22 answers)
* Q24, do you have any other thoughts about data in the voluntary sector (30 answers)

