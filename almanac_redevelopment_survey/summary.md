# Summary of results from the Almanac Redevelopment Survey

## Q10 - What kinds of data do you use specifically?
* State of the overall sector (number charities, information about each charity, number grouped by type/category)
* Sector finances (income broken down by source and type, expenditure broken down by type, assets broken down by type)
* Geography (the information from the two points above but zoomed in to the area. Similar to the economic and social information point below,m but knowing about the state of the voluntary sector in that area rather than knowing about the economic/social state)
* What similar organisations there are (to know how many are doing similar things, networking purposes)
* Domain specific (examples given include local community transport data, disability data, number of complains/whistleblowing reports)
* Funding and grants information (broken down in a similar way such as through type)
* Employees (demographics, satisfaction, salaries)
* Volunteers & trustees (demographics, satisfaction)
* Impact (wide range of examples of impact data given, bullet point some below:
  * NPS (net promoter score)
  * Programme monitoring
  * Surveys
  * Internal data from their own CRM
  * Output and outcome data
  * Social and economic impact
  * Ensuring demand is being met
* Economic and Social information of the UK (information from ONS about the economic/social landscape e.g. poverty/deprivation index, can apply this more broadly as what do people in a certain area need)
* Website and Marketing (google analytics information about clicks and email views etc)

## Q11 - When you want to have access to data about the voluntary sector, where do you go?
* Charity Commission
* NCVO Almanac
* 360 Giving
* Government data
  * levelling up areas
  * ONS
  * CIC regulator
  * companies house
  * 
* Domain specific sources, local specific sources
* Thinktanks such as NPC or PBE
* Harris Hill
* Charity Jobs
* National bodies: NCVO, IFS
* Benchmarking websites:
  * https://2021.mrbenchmarks.com/uk/fundraising
  * https://www.charitybenchmarks.org/
  * https://ciof.org.uk/events-and-training/resources/charity-benchmarks-sector-report-2019
* CharityBase
* ACF Foundation giving trends
* Charity Finance Group

## Q21, what data did you need that you were unable to find when you used the almanac before (26 answers)
* Regional breakdwon
* Local social and economic impact
* Staffing: salary and benefit data, turnover, number of staff, wellbeing, satisfaction, EDI
* Grants
* Long time series data
* 


## Q23, how could we make the almanac more useful to you (22 answers)
* Group charities by their number of employees
* Add equalities data
* Make it easier to use raw data
* Add output/outcome data alongside financial data
* Locality data, be able to dig deeper into a smaller geographic area
* More up to date
* Continue to report on distinction between grants and contracts

## Q24, do you have any other thoughts about data in the voluntary sector
* Some people don't know about the almanac, and suggesting improving its visibility in search engines
* Consistent data collection with standard definitions (civil society, charities, general charities, non-profit)
* The sector needs better data capacity - skills, training, resources
* Standardise impact data
* It would be useful to have a one stop shop for everything