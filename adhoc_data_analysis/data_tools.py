import pandas as pd


def transform_to_one_transaction_per_row(accounts_sample, transaction_cols):
    fixed_cols = list(set(accounts_sample.columns) - set(transaction_cols))
    return pd.melt(accounts_sample, id_vars=fixed_cols, value_vars=transaction_cols, var_name="transaction")