# Upload Almanac Data to UK Data Service

Since the data entry for the Almanac is funded by ESRC, part of this requirement is that we upload the data to the UK Data Service within three months of the funding ending.

The data required for the 2021-2023 Almanac upload can be created by running the `script.ipynb` notebook.