# Sample Scenario Estimates

In this folder, we will do any analysis for calculating how confident we can be in our results for different sample scenario estimates.

Currently we have the notebook "sample_scenario_estimate_analysis.ipynb" that looks to calculate confidence levels for different sampling scenarios. This is the notebook for the Trello Card [AN002](https://trello.com/c/IdOZ2OUa/2-an002-analysis-the-impact-of-different-sample-sizes).

Development for this was done in Python 3.8.