# Clinks

This folder is for any analysis of Clinks data. This work was previously being saved on the G drive, however I am now looking to save all code in Bitbucket so that it is under source control.

The notebook "2022_03_31_AM_Clinks_20222_analysis.ipynb" is the analysis that Anya did originally. The notebook runs from start to finish. If you would like to do the analysis for Welsh charities only, then you need to uncomment the penultimate line of box 6. This also requires running the script "welsh_postcodes.R" beforehand.


