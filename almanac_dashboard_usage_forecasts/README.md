# Almanac Dashboard Usage Forecasts

This directory is used to estimate how much usage we expect the redeveloped almanac to have.

A few key questions to answer are:

* What is the maximum number of users we expect at any one time?
* How much time do we expect there to be at least one user using the dashboard?
* What is the average maximum number of users we expect during a weekday?

Development work was done in R 4.2.

To rerun the reports, you need to add the excel files which are reports downloaded from google analytics to the `data/external/` directory in this project. They are saved on sharepoint at the following link: `https://ncvo.sharepoint.com/sites/NETWORKSINFLUENCING/Shared%20Documents/Rsearch/Almanac/Almanac%20Development/Almanac%20Redevelopment/Google%20Analytics%20Almanac%20Usage/Almanac%20page%20analytics%202022.09.20-2023.02.20.xlsx`.