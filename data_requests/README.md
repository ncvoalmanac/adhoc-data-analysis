# Data Requests

This is the directory for any data requests that we receive from external people for our data. Ideally the code to complete each separate data request should be in a Jupyter notebook.