from setuptools import setup, find_packages

setup(
    name="adhoc_data_analysis",
    packages=find_packages()
)